import nlp_functions

def printResultChoice():
    userChoice = str(input('\nPrint the result? (Y/N) :'))
    if(userChoice=='Y' or userChoice=='y'):
        return True
    else:
        return False

#_FolderName='Data\\OppoF1\\'
_FolderName='data\\'
_ReviewDataset=_FolderName+'data.txt'
_PreProcessedData=_FolderName+'1.pre_processed_data.txt'
_TokenizedReviews=_FolderName+'2.tokenized_reviews.txt'
_PosTaggedReviews=_FolderName+'3.pos_reviews.txt'
_Aspects=_FolderName+'4.aspects.txt'
_Opinions=_FolderName+'5.opinions.txt'


input("Please Enter any key to continue")

# PREPROCESSING DATA
nlp_functions.preProcessing(_ReviewDataset,_PreProcessedData,printResultChoice())


# READING COLLECTION
nlp_functions.tokenizeReviews(_ReviewDataset,_TokenizedReviews,printResultChoice())


# PART OF SPEECH TAGGING
nlp_functions.posTagging(_TokenizedReviews,_PosTaggedReviews,printResultChoice())

# This function will list all the nouns as aspect
nlp_functions.aspectExtraction(_PosTaggedReviews,_Aspects,printResultChoice())


# IDENTIFYING OPINION WORDS
nlp_functions.identifyOpinionWords(_PosTaggedReviews,_Aspects,_Opinions,printResultChoice())