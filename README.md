<div id="top"></div>


<!-- PROJECT SHIELDS 
[![Forks][forks-shield]][forks-url]
[![Issues][issues-shield]][issues-url]
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Tim-AI-Projects/nlp-opinion-mining-system/-/edit/master/README.md">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Opinion Mining System</h3>

  <p align="center">
    Using a pre-trained AI to understand the opinion of text.
    <br />
    <a href="https://gitlab.com/Tim-AI-Projects/nlp-opinion-mining-system/-/edit/master/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
    <img src="images/logo.jpg" alt="Logo">
</div>

### NLP
This project contains a NLP prototype for the group project. 

It can do the following
- It can remove stopping words like; will, we, our, me etc.. These words are not needed when looking for sentiment.  
- Split the text in sentences.
- Check if text is positive or negative
- It is NOT tested on sarcasm

### More
- Documentation about this project can be found here: [NLP Prototype](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EUOeAwAU54BClCC4bPTp99EBgWFcnYnprYgfMMX9qSK0gA?e=wqH9hN) and here [NLP Research](https://docs.google.com/document/d/1jiYt6s0o4MdMwFKsDiH5CNRJIlpS7Bmf/edit)
- The personal challenge project plan can be found here: [Project plan](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EUkskXcqbR9Mn2nJwiRHCFAB7wu-7nJsoLH3onf03pCoFQ?e=dWOBTe)
- The other repositories can be found here: [Tim AI Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This section lists any major frameworks/libraries used for the project.

* [Python](https://www.python.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

All the things you need to setup before starting.

* A Windows 10 PC
* Install [Python >= 3.7](https://realpython.com/installing-python/) 
* [Visual Studio Code (or another IDE)](https://code.visualstudio.com/)

### Installation

_Below is an example of how you can install and setup the app._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Tim-AI-Projects/nlp-opinion-mining-system.git
   ```
2. Install pip packages
   ```sh
    - pip install -r requirements.txt
    - python -m nltk.downloader stopwords
    - python -m nltk.downloader punkt
    - python -m nltk.downloader averaged_perceptron_tagger
    - python -m nltk.downloader wordnet
    - python -m nltk.downloader sentiwordnet
   ```
3. Add your text
    ```sh
    In the 'data' folder, create a text file (or edit the existing one) named data.txt with your text, or change the code to fetch data.
    ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

This shows an example of how to use the app.

To run the app use: 
```sh
python nlp.py
```

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/Tim-AI-Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/forks/new
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/blob/main/LICENSE.txt
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/issues
